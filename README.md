# Personal documentation repository

The goal of this repository is to provide the specific documentation of solution I developed.
Feel free to use the documentation, modify it and request what you would like.

## HowTo use the custom guix packages

The first thing to do, is to create the following file `~/.config/guix/channels.scm`:

```
(cons* (channel
          (name 'gricad-guix-packages)
          (url "https://gricad-gitlab.univ-grenoble-alpes.fr/bouttiep/gricad_guix_packages.git")
          (branch "WIP_Benjamin"))
       %default-channels)
```

Once this is done, you can load and update the guix environment:

```bash
source /applis/site/guix-start.sh
guix pull  # This will take a while
```

> You only need to update the guix environment (and thus run `guix pull`) when a package you want to use has been created or updated.

After `guix pull`, you should run the following command to be sure you use the lastest `guix` command: 

```
GUIX_PROFILE="$HOME/.config/guix/current"
. "$GUIX_PROFILE/etc/profile"
```

Finally, I recommend that you to use manifest to run the package you want. This will load a custom environment with the package specified in the manifest.scm file. 

```bash
guix shell --pure -m manifest.scm
```
