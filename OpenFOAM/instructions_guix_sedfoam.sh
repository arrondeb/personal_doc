# source OpenFOAM environment
source $1/share/OpenFOAM/etc/bashrc

## Execute the tutorial ...
# create the mesh
foamCleanPolyMesh
blockMesh

# create the intial time folder
cp -r ./0_org ./0

# create C fles for post-processing
postProcess -func writeCellCentres -constant

# Decompose the case in order to run in parallel (on 16 cores)
decomposePar

export MYPATH=/home/arrondeb/test_OpenFoam/tutorials/LES/3DChannel560

# Run sedFoam in parallel
mpirun -np `cat $MYPATH/machinefile | wc -l` \
        --machinefile $MYPATH/machinefile \
        --prefix $1 \
        $1/bin/bash -c "source $1/share/OpenFOAM/etc/bashrc; sedFoam_rbgh -parallel"

