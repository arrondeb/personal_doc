#!/bin/bash
#OAR -n run_apptainer_image
#OAR -l /nodes=2/core=2,walltime=00:30:00
#OAR --project pr-test

# First, we activate a Nix profile with openmpi that has been installed
source /applis/site/nix_nur.sh
nix-env --switch-profile $NIX_USER_PROFILE_DIR/profile_ompi

# Note that this can be replaced by loading a guix environment with openmpi
# source /applis/site/guix_start.sh

# set the parameters for OpenMPI
export OMPI_MCA_btl_openib_allow_ib=true
export OMPI_MCA_pml=cm
export OMPI_MCA_mtl=psm2

# launch an apptainer instance on each compute node
mpirun -npernode 1 \
       --machinefile $OAR_NODE_FILE \
       -mca btl_openib_allow_ib "true" \
       -mca plm_rsh_agent "oarsh" \
       --prefix $HOME/.nix-profile \
       /usr/bin/apptainer instance start \
       --bind ${PWD}/:/mnt/cases \
       ${PWD}/sedfoam_v2106_ubuntu_latest.sif openfoam_instance

# run the code/script with the MPI command
mpirun -np `cat $OAR_FILE_NODES|wc -l` \
        --machinefile $OAR_NODE_FILE \
        -mca plm_rsh_agent "oarsh" \
        --prefix $HOME/.nix-profile \
        /usr/bin/apptainer exec \
        instance://openfoam_instance \
        /bin/bash -c "source /usr/lib/openfoam/openfoam2212/etc/bashrc; decomposePar; buoyantBoussinesqPimpleFoam; -case /mnt/cases; -parallel"


mpirun -npernode 1 \
        --machinefile $OAR_NODE_FILE \
        -mca btl_openib_allow_ib "true" \
        -mca plm_rsh_agent "oarsh" \
        --prefix $HOME/.nix-profile \
        /usr/bin/apptainer instance stop openfoam_instance
