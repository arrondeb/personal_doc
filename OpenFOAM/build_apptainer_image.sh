#!/bin/bash
#OAR -n build_apptainer_image
#OAR -l /nodes=1/core=32,walltime=00:30:00
#OAR --project pr-test

# Pull and build the Apptainer image from dockerhub with Apptainer v1.3.2
# This is creating the image sedfoam_v2106_ubuntu_latest.sif in the current directory
/usr/bin/apptainer pull docker://cbonamy/sedfoam_v2106_ubuntu
