;; Ce qui suit est un "manifeste" équivalent à la ligne de commande que vous avez donnée.
;; Vous pouvez le stocker dans un fichier que vous pourrez ensuite passer à n'importe quelle
;; commande 'guix' qui accepte une option '--manifest' (ou '-m').

(specifications->manifest
  (list "sedfoam"
        "openfoam-custom"
	"swak4Foam"
	"openmpi"
        "gcc-toolchain@11"
        "coreutils"
	"bash"
	"sed"
	"make"))
