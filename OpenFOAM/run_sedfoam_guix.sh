#!/bin/bash
#OAR -n sedfoam_multinodes
#OAR -l /nodes=4/core=32,walltime=00:30:00
#OAR --project pr-test

# load guix session
source /applis/site/guix-start.sh

# set the parameters for OpenMPI
export OMPI_MCA_plm_rsh_agent=/usr/bin/oarsh
export OMPI_MCA_btl_openib_allow_ib=true
export OMPI_MCA_pml=cm
export OMPI_MCA_mtl=psm2

ENV="`guix shell -m /home/arrondeb/test_OpenFoam/sedfoam.scm -- /bin/sh -c 'echo $GUIX_ENVIRONMENT'`"

# Here is an example of project directory 
export projectDir=/home/arrondeb/test_OpenFoam/tutorials/LES/3DChannel560/
cd $projectDir

# Fix issue occuring with OAR files not accessible by openmpi ...
cat $OAR_FILE_NODES > ./machinefile

# Invoke guix shell with sedfoam environment to run the tutorial
# The instructions_guix_sedfoam.sh file should be in $projectDir folder
exec ~/.config/guix/current/bin/guix shell --pure -E ^OMPI -E ^OAR -E ^OMP -E ^WM -E ^FOAM \
      -m /home/arrondeb/test_OpenFoam/sedfoam.scm \
      -- ./instructions_guix_sedfoam.sh $ENV
