# Maintain your own guix package

This tutorial does not aim to help you with the basics of guix. This tutorial is intended to help advanced users maintain their own guix package or create new ones from the gitlab repository (https://gricad-gitlab.univ-grenoble-alpes.fr/bouttiep/gricad_guix_packages). In general, I recommend that you create a new branch for your tests. I will use the sedfoam guix package (version 2312) as an example in the following sections.

## HowTo update a current package

This section is only relevant if you have updated the current code on which the guix package is based. The changes are small and do not require a new release for your code. Therefore, only a small modification to the current guix package is required. To do this, you need to find the following lines in the sedfoam.scm file:

```
(define-public sedfoam
  ;; This is a fork of 'openfoam-org', maintained separately.
  (package
    (name "sedfoam")
    (version "2312")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/SedFoam/sedfoam")
              (commit "6c1629b1c3d586ebe43565f589b30a24c1d53fa3")
              (recursive? #t)))
        (sha256 (base32 "1dmrmxmbipfcy64b5h2ydq8vmxy927lbklrfjxd6ljla87c0r99j"))))
	...
```

Now you need to find the commit number associated with the "new" version of your code, and replace the "commit" field. Then you need to change the checksum identifier (the "sha256" field) associated with that particular commit. To get it, simply run the following command in a terminal and copy and paste the result into the "base32" field:

```bash
guix download -r --commit=new_commit_number https://github.com/SedFoam/sedfoam
```

> To make sure you have the correct checksum, run this command several times until the output is the same.  

> The `-r` option is only relevant if the submodules are downloaded automatically. To find out if this is the case for your package, look at the "recursive? By default, submodules are not downloaded.

## HowTo create a new guix package

This section is relevant if your code has changed significantly and/or you have released a new version. In this case, it is appropriate to keep the previous guix package and make a new one related to the new release. To do this, you need to copy and paste the previous guix package definition, delimited like this:

```
(define-public sedfoam
  (package
    (name "sedfoam")
    (version "2312")
    	...
    (home-page "https://sedfoam.github.io/sedfoam/")
    (license license:gpl3+)))
```

Then you need to change the package name (after the "define-public" field), the version, the commit and the checksum. For the package name, the convention would be to make it "package_name-version" for clarity. To change the commit and checksum, see the previous section (#HowTo update a current package). 

## What is next?

Once you are happy with the updated/new package, I recommend asking for a pull request on the gitlab repository (https://gricad-gitlab.univ-grenoble-alpes.fr/bouttiep/gricad_guix_packages).
