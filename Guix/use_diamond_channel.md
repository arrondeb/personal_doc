## Comment configurer une connexion SSH transparente aux clusters Gricad ?

La première chose à faire est de générer une clé RSA. Pour ce faire, exécutez la commande :
```bash
ssh-keygen
```

> Ici, vous aurez à définir un mot de passe (à renseigner une seule fois par session)

Ensuite, il vous suffit de créer le `~/.ssh/config` comme suit :

```bash
Host *
  ServerAliveInterval 30

Host *.ciment
  User <your-perseus-login>
  ProxyCommand ssh -q <your-perseus-login>@access-gricad.univ-grenoble-alpes.fr "nc -w 60 `basename %h .ciment` %p"
```

> Veillez à remplacer `<your-perseus-login>` par votre login perseus ...

Puis, pour finaliser la configuration de la connexion SSH transparente, vous devez copier la clé RSA sur les frontales et les clusters avec les commandes suivantes :

```bash
ssh-copy-id <your-perseus-login>@rotule.univ-grenoble-alpes.fr
ssh-copy-id <your-perseus-login>@trinity.univ-grenoble-alpes.fr
ssh-copy-id dahu.ciment
```

> Attention, vous devrez renseigner le mot de passe de votre compte PERSEUS à chaque étape.

Pour vérifier que tout fonctionne correctement, vous pouvez essayer de vous connecter au cluster `dahu` (avec la commande `ssh dahu.ciment`).

Sur les postes en libre accès à PHELMA, vous ne possèdez pas les droits super-utilisateurs. Vous ne pouvez donc pas créer le fichier `~/.ssh/config`. Il est donc conseillé de créer ce fichier dans un répertoire ou vous possédez les droits (`~/ssh_config` par exemple). Vous devez ensuite renseigner ce path à chaque commande `ssh` ou `ssh-copy-id` (via l'argument `-F <path-of-ssh-config-file>`).

## Comment accéder aux paquets Guix du canal Diamond ?

Quand vous utilisez guix, l'ensemble des paquets disponibles proviennent [du channel guix par défaut.](https://hpc.guix.info/browse)

Pour ajouter le canal Diamond et donc pouvoir installer les paquets à l'aide de la commande `guix install`, il faut créer un fichier `~/.config/guix/channels.scm` qui contiendra les lignes suivantes : 

```
;; Add gricad packages to those Guix provides.
(cons (channel
        (name 'guix-channel)
        (url "https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/guix/guix-channel.git"))
      %default-channels)
```

Une fois ceci fait, lancez la commande `guix pull`. 

Après, le `guix pull`, comme indiqué en sortie, exécutez les commandes suivantes, pour être sûr d'utiliser la commande `guix` à jour : 

```
arrondeb@f-dahu:~$ GUIX_PROFILE="$HOME/.config/guix/current"
arrondeb@f-dahu:~$ . "$GUIX_PROFILE/etc/profile"
```

Pour vérifier que vous avez bien accès aux paquets diamond, vous pouvez tenter de rechercher le paquet `n2p2` :

```
arrondeb@f-dahu:~$ guix search n2p2
name: n2p2
version: 2.1.4                                                         
outputs:
+ out: everything
systems: x86_64-linux
dependencies: eigen@3.4.0 gsl@2.7.1 openblas@0.3.20 openmpi@4.1.6
location: custom/n2p2.scm:16:1
homepage: https://compphysvienna.github.io/n2p2/
license: GPL 3+
synopsis: Neural network potentials for chemistry and physics
description: This package contains software that will allow you to use existing neural network potential parameterizations to predict energies and forces (with standalone tools but
+ also in conjunction with the MD software LAMMPS).  In addition it is possible to train new neural network potentials with the provided training tools.
relevance: 35
```

Ici, l'output de la commande n'est pas nul donc vous avez bel et bien accès aux paquets du canal Diamond. Il ne vous reste plus qu'à installer ceux dont vous avez besoin avec la commande `guix install`:

```
arrondeb@f-dahu:~$ guix install n2p2 lammps@stable_29Aug2024_update1
```

Cette commande (un peu longue) installe les paquets `n2p2` et `lammps` sur votre profile guix. Vous aurez donc accès aux binaires de ces codes dans le dossier `~/.guix-profile/bin`.
