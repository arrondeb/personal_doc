export MYPATH=/home/arrondeb/test_fluiddyn

# Then, in parallel
mpirun -np `cat $MYPATH/machinefile | wc -l` \
        --machinefile $MYPATH/machinefile \
        --prefix $1 \
        fluidfft-bench 256 -d 3
