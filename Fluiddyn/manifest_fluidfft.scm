;; Ce qui suit est un "manifeste" équivalent à la ligne de commande que vous avez donnée.
;; Vous pouvez le stocker dans un fichier que vous pourrez ensuite passer à n'importe quelle
;; commande 'guix' qui accepte une option '--manifest' (ou '-m').

(specifications->manifest
  (list "python-fluidfft"
	"python-fluidfft-builder"
	"python-fluidfft-fftw"
	"python-fluidfft-fftwmpi"
	"python-fluidfft-mpi-with-fftw"
	"python-fluidfft-p3dfft"
	"python-fluidfft-pfft"
        "python"
	"python-pytest"
	"coreutils"
	"openmpi"
        ))
