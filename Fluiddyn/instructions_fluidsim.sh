# First, pytest in serial
pytest --pyargs --color=no fluidsim

export MYPATH=/home/arrondeb/test_fluiddyn

# Then, in parallel
mpirun -np `cat $MYPATH/machinefile | wc -l` \
        --machinefile $MYPATH/machinefile \
        --prefix $1 \
        pytest --pyargs --color=no --timeout=300 fluidsim
